#version 450

layout(location=0) out vec4 FragColor;

in vec2 v_TexCoord;

float c_PI = 3.141592;

// uniform vec2 u_InputPoints[10];

uniform float u_Time = 0.0;

void main()
{
	// for (all u_InputPoints[i] draw circle)
	
	//float distance = distance(v_TexCoord, vec2(0.5,0.5));
	/*if(0.49 < distance && distance < 0.50)
		FragColor = vec4(1);
	else
		FragColor = vec4(0);*/
		
	/*float scalar = pow(sin(distance*4.0*c_PI*5),12);
	FragColor = vec4(scalar);*/

	vec2 inputPoint = vec2(0.25, 0.0);
	float inputPointSize = 0.05;
	
	int isCircleLineRegion = 0;

	float distance1 = distance(v_TexCoord, vec2(0.75,0.5));
	float comparDis = fract(u_Time);
	if(comparDis-0.01 < distance1 && distance1 < comparDis){
		isCircleLineRegion = 1;
		FragColor = vec4(0.5);
		}
	else{
		FragColor = vec4(0);
	}

	if (isCircleLineRegion > 0){
		float distance0 = distance(v_TexCoord, vec2(0.5,0.5));
		if ( distance0< inputPointSize){
			FragColor += vec4(1.0);
		}
	}
}
