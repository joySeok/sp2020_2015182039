#version 450

in vec3 a_Position;		// vs input ����, float 3��, in->vs input -> attrib

out vec2 v_TexCoord;

uniform float u_Time;

float c_PI = 3.141592;

void main()
{
	float newX = a_Position.x + 0.5;
	float newY = a_Position.y + 0.5;
	v_TexCoord = vec2(newX,newY);

	gl_Position = vec4(a_Position,1.0);
}
