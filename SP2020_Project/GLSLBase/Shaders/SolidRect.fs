#version 450

layout(location=0) out vec4 FragColor;

in vec4 v_Color;	// fragment shader input

void main()
{
	FragColor = vec4(v_Color);
}
